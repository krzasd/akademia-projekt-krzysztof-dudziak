﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MojProjekt
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            /*
             * Rozpoczęcie gry
             */ 
            GameWindow win2 = new GameWindow();
            win2.Show();
            this.Close();
        }

        private void exitButton_Click(object sender, RoutedEventArgs e)
        {
            /*
             * Wyjście
             */ 
            Application.Current.Shutdown();
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            /*
             * Wyświetlanie instrukcji
             */ 
            MessageBox.Show(@"
        Controls
            <- - move to left
            -> - move to right
            Space - shoot (because otherwise it would be to simple, after a shot
                        the tank will stop and in order to move it, pressing 
                        left or right is required.)
        Rules
            1. This game objective is to get as much points as possible
            2. You get 20 points for shooting circle, 40 for square and 60 for rectangle
            3. If the object manages too fall onto the ground, you get -20 points 
                for circle, -40 for square and -60 for rectangle
            4. If your total amount of points drops beneath 0, game is over");
        }
    }
}
