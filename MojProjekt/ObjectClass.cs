﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Shapes;

namespace MojProjekt
{
    abstract class ObjectClass
    {
        protected int x = 0;
        protected int y = 0;
        protected Shape shape;
        protected double pointValue = 20;

        //propercje
        public int X
        {
            get { return this.x; }
            set { this.x = value; }
        }
        public int Y
        {
            get { return this.y; }
            set { this.y = value; }
        }
        public double getPointValue
        {
            get { return this.pointValue; }
        }
        public Shape ReturnShape
        {
            get { return this.shape; }
        }

        public abstract void move(int up, int down, int left, int right);
        public abstract void draw(GameWindow window);
        public abstract bool isTouched(ObjectClass collidingObject);
    }
}
