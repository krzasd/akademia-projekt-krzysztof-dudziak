﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Shapes;
using System.Windows.Media;

namespace MojProjekt
{
    class Circle : ObjectClass //dziedziczenie
    {
        private int radius;

        public Circle(GameWindow window)
        {
            x = 0;
            y = 0;
            radius = 1;
            base.pointValue = 20;
        }
        public Circle(GameWindow window, int xValue, int yValue, int radiusValue)
        {
            x = xValue;
            y = yValue;
            radius = radiusValue;
            base.pointValue = 20;
            shape = new Ellipse();
            shape.Margin = new System.Windows.Thickness(x, y, 0, 0);
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            mySolidColorBrush.Color = Color.FromArgb(255, 255, 255, 0);
            shape.Fill = mySolidColorBrush;
            shape.StrokeThickness = 2;
            shape.Stroke = Brushes.Black;
            shape.Width = 2 * radius;
            shape.Height = 2 * radius;
            window.grid1.Children.Add(shape);
        }


        public override bool isTouched(ObjectClass collidingObject)
        {
            /*
             * Wykrywanie zderzenia
             */ 
            if (x - collidingObject.ReturnShape.Width < collidingObject.X && x + 2 * radius > collidingObject.X)
                if ( y+(2 * radius) > collidingObject.Y)
                    return true;
            return false;
        }
        public override void move(int up, int down, int left, int right)
        {
            /*
             * Przemieszczenie się
             */ 
            y -= up;
            y += down;
            x += right;
            x -= left;
        }
        public override void draw(GameWindow window)
        {
            /*
             * Rysowanie
             */
            shape.Margin = new System.Windows.Thickness(x, y, 0, 0);
        }
    }
}
