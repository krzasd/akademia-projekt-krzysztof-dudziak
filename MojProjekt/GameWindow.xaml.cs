﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Timers;

namespace MojProjekt
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Window
    {
        private Ship tank;
        Timer timer;

        //listy ukazujące w zastosowaniu polimorfizm
        private List<ObjectClass> listOfEnemies; 
        private List<ObjectClass> listOfMissiles;

        private int enemiesPerCycle = 3;
        private int cycleBufferFillCounter = 0;
        private int cycleBuffer = 200;
        private int cycleCounter = 0;
        private double pointCounter = 0;
        private double bestScore = 0;
        private int difficulty = 1000;
        private int speedEnemy = 1;

        private enum Shapes { Circle, Square, DoubleSquare }; //enum

        public GameWindow()
        {
            /*
             * Wystartowanie okna
             */
            InitializeComponent();
            listOfEnemies = new List<ObjectClass>();
            listOfMissiles = new List<ObjectClass>();
            tank = new Ship();
            tank.X = (int)image1.Margin.Left;
            tank.Y = (int)image1.Margin.Top;
            timer = new Timer(10);
            timer.Elapsed += new ElapsedEventHandler(Window_TimerElapsed);
            timer.Start();
            
        }
        
        private void Window_TimerElapsed(object sender, EventArgs e)
        {
            /*
             * Obsługa timera
             */
            this.Dispatcher.Invoke((Action)(() =>
            {
                turn();
                InvalidateVisual();
                if (pointCounter > bestScore)
                    bestScore = pointCounter;
                if (pointCounter < 0)
                {
                   
                    timer.Stop();
                    timer.Elapsed -= Window_TimerElapsed;
                    MessageBox.Show("Game over\nBestScore: " + bestScore);
                    MainWindow win2 = new MainWindow();
                    win2.Show();
                    this.Close();
                }
            }));
            
        }

        private void turn()
        {
            /*
             * Mechanizm gry
             */ 
            ++cycleCounter;
            if (cycleBuffer > 1)
            {
                int oldCycleBuffer = cycleBuffer;
                cycleBuffer -= 10*cycleCounter / difficulty;
                if (oldCycleBuffer != cycleBuffer)
                    cycleCounter = 0;
            }
            image1.Margin = new Thickness(tank.X, tank.Y, 0, 0);
            missileHandling();
            enemiesHandling();
            pointLabel.Content = pointCounter;
            if (cycleBufferFillCounter < cycleBuffer)
            {
                ++cycleBufferFillCounter;
                return;
            }
            cycleBufferFillCounter = 0;
            generateEnemies();
                
        }

        private void missileHandling()
        {
            /*
             * Sterowanie pociskami i usuwanie ich
             */
            try
            {
                for (int i = listOfMissiles.Count() - 1; i >= 0; --i)
                {
                    listOfMissiles[i].move(20, 0, 0, 0);
                    listOfMissiles[i].draw(this);
                    if (listOfMissiles[i].Y < -(int)this.ActualHeight)
                    {
                        grid1.Children.Remove(listOfMissiles[i].ReturnShape);
                        listOfMissiles.RemoveAt(i);
                    }
                }
            }
            catch (IndexOutOfRangeException exc) { }
            catch (ArgumentOutOfRangeException exc) { }
        }

        private void enemiesHandling()
        {
            /*
             * Sterowanie przeciwnikami, usuwanie ich i naliczanie punktów
             */ 
            try //wyjątki
            {
                for (int i = listOfEnemies.Count() - 1; i >= 0; --i)
                {
                    if(listOfEnemies[i] is DoubleSquare )
                        listOfEnemies[i].move(0, speedEnemy+1, 0, 0);
                    else
                        listOfEnemies[i].move(0, speedEnemy, 0, 0);
                    listOfEnemies[i].draw(this);
                    if (listOfEnemies[i].Y > ((int)this.ActualHeight - (int)image1.ActualHeight))
                    {
                        pointCounter -= (listOfEnemies[i].getPointValue);
                        grid1.Children.Remove(listOfEnemies[i].ReturnShape);
                        listOfEnemies.RemoveAt(i);

                    }
                    for (int j = listOfMissiles.Count() - 1; j >= 0; --j)
                    {
                        try
                        {
                            if (listOfEnemies[i].isTouched(listOfMissiles[j]))
                            {
                                pointCounter += (listOfEnemies[i].getPointValue);
                                grid1.Children.Remove(listOfEnemies[i].ReturnShape);
                                listOfEnemies.RemoveAt(i);
                                grid1.Children.Remove(listOfMissiles[j].ReturnShape);
                                listOfMissiles.RemoveAt(j);

                            }
                        }
                        catch (IndexOutOfRangeException except) { }
                        catch (ArgumentOutOfRangeException except) { }
                    }
                }
            }
            catch (IndexOutOfRangeException exc) { }
            catch (ArgumentOutOfRangeException exc) { }
        }

        private void generateEnemies()
        {
            /*
             * Generowanie przeciwników i umieszczanie ich na właściwych pozycjach
             */ 
            Random randomValue = new Random();

            for (int i = 0; i < enemiesPerCycle; ++i)
            {
                int value1 = randomValue.Next();
                int value2 = randomValue.Next(0, 1000);
                ObjectClass newObject = new Circle(this);
                int posX = (value1 % (2 * (int)this.ActualWidth)-(int)image1.Width-300) - (int)this.ActualWidth+(int)image1.Width+150;
                int posY = (value2 % ((int)this.ActualHeight / 2)) - (int)this.ActualHeight;
                switch (value1 % 3)
                {
                    case (int)Shapes.Circle:
                        newObject = new Circle(this, posX, posY, 8);
                        break;
                    case (int)Shapes.Square:
                        newObject = new Square(this, posX, posY, 16);
                        break;
                    case (int)Shapes.DoubleSquare:
                        newObject = new DoubleSquare(this, posX, posY, 16);
                        break;
                }
                listOfEnemies.Add(newObject);
            }
        }

        private void Window_KeyDown_1(object sender, KeyEventArgs e)
        {
            /*
             * Obsługa klawiatury
             * UWAGA : zatrzymanie się po naciśnięciu spacji jest zamieżone
             */ 
            int speed = 10;
            if (e.Key == Key.Left && (tank.X - speed > -image1.Width)) 
                tank.move(0, 0, speed, 0);
            if (e.Key == Key.Right && (tank.X + speed < this.ActualWidth))
                tank.move(0, 0, 0, speed);
            if (e.Key == Key.Space)
                listOfMissiles.Add(new Circle(this, 2*tank.X - (int)this.ActualWidth+(int)image1.Width+2, tank.Y, 5));
        }
        
    }
}
