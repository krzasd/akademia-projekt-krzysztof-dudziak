﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Shapes;

namespace MojProjekt
{
    class Ship : ObjectClass //dziedziczenie
    {
        public override void move(int up, int down, int left, int right)
        {
            /*
             * Przemieszczenie się
             */ 
            y -= up;
            y += down;
            x += right;
            x -= left;
        }

        //funkcje przydatne do dalszych modyfikacji gry
        public override void draw(GameWindow window){}
        public override bool isTouched(ObjectClass collidingObject)
        {
            return false;
        }
    }
}
