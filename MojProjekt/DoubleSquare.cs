﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Shapes;
using System.Windows.Media;

namespace MojProjekt
{
    class DoubleSquare : ObjectClass //dziedziczenie
    {
        private int side;

        public DoubleSquare(GameWindow window)
        {
            x = 0;
            y = 0;
            side = 1;
            base.pointValue = 60;
        }
        public DoubleSquare(GameWindow window, int xValue, int yValue, int sideValue)
        {
            x = xValue;
            y = yValue;
            side = sideValue;
            base.pointValue = 60;
            shape = new Rectangle();
            shape.Margin = new System.Windows.Thickness(x, y, 0, 0);
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            mySolidColorBrush.Color = Color.FromArgb(255, 255, 255, 0);
            shape.Fill = mySolidColorBrush;
            shape.StrokeThickness = 2;
            shape.Stroke = Brushes.Black;
            shape.Width = side;
            shape.Height = side*2;
            window.grid1.Children.Add(shape);
         }

        public override bool isTouched(ObjectClass collidingObject)
        {
            /*
             * Wykrywanie zderzenia
             */ 
            if (x - collidingObject.ReturnShape.Width < collidingObject.X && x + side > collidingObject.X)
                if (y + 2*side > collidingObject.Y)
                    return true;
            return false;
        }
        public override void move(int up, int down, int left, int right)
        {
            /*
             * Przemieszczanie się
             */ 
            y -= up;
            y += down;
            x += right;
            x -= left;
        }
        public override void draw(GameWindow window)
        {
            /*
             * Rysowanie
             */
            shape.Margin = new System.Windows.Thickness(x, y, 0, 0);
        }
    }
}
